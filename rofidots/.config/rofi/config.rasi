/*configuration {
    modi: "window,run,ssh,drun";
    font: "JetBrainsMono Nerd Font 10";
    icon-theme: "Candy-icons";


    display-drun:   " ";
    drun-display-format: "{name}";

    display-window: "Trocar";
    display-ssh:    "SSH";

    matching: "normal";
    window-format: "{t}";
    show-icons:     true;
    sidebar-mode:   false;
}

@import "./colorscheme.rasi"

* {
    background-color: @background;
    text-color:       @white;
    border-color:     @border;
    spacing: 0;
    width: 700px;
    height: 700px;
}

window {
    border-radius: 20px;
    transparency: "real";
    border: 2px;
}

inputbar {
    border: 0 0 1px 0;
    padding: -2px;
}

prompt {
    padding: 16px;
    border: 0px 1px 0 0;
}

entry {
    padding: 16px;
    placeholder: "Search";
    blink: true;
}

mainbox {
    children: [inputbar, listview];
    margin: 0 4px 0 4px;
}

listview {
    columns: 3;
    layout: vertical;
    spacing: 0;
    margin: 1% 0;
}

element {
    orientation: vertical;
    padding: 1.5% 1.5%;
    border-radius: 1;
}

element normal.urgent, element alternate.urgent {
    background-color: @urgent;
}

element normal.active, element alternate.active {
    background-color: @background-alt;
}

element element.alternate.normal {
    background-color: @background;
    text-color:       @background;
}

element selected {
    background-color: @background-alt;
}

element selected.urgent {
    background-color: @urgent;
}

element selected.active {
    background-color:  @background-alt;
}

element-icon {
    size: 3%;
}

element-text {
    color: inherit;
    horizontal-allign: 0.5;
}

element selected {
    background-color: @background-alt;
} */

/*
███╗░░░███╗░█████╗░██████╗░████████╗██╗░░░██╗
████╗░████║██╔══██╗██╔══██╗╚══██╔══╝╚██╗░██╔╝
██╔████╔██║███████║██████╔╝░░░██║░░░░╚████╔╝░
██║╚██╔╝██║██╔══██║██╔══██╗░░░██║░░░░░╚██╔╝░░
██║░╚═╝░██║██║░░██║██║░░██║░░░██║░░░░░░██║░░░
╚═╝░░░░░╚═╝╚═╝░░╚═╝╚═╝░░╚═╝░░░╚═╝░░░░░░╚═╝░░░
*/

/*----- Configuration -----*/
configuration {
  font: "JetBrains Mono Nerd Font 12";
	modi:                       "window,run,ssh,drun";
    show-icons:                 true;
    display-drun:               " ";
	drun-display-format:        "{name}";

    display-window: "Trocar";
    display-ssh:    "SSH";

    matching: "normal";
    window-format: "{t}";
    sidebar-mode:   false;
}

/*----- Global Colorscheme -----*/
@import                          "colorscheme.rasi"

/*****----- Main Window -----*****/
window {
    transparency:                "real";
    fullscreen:                  false;
    width:                       600px;
    x-offset:                    20px;
    y-offset:                    0px;

    enabled:                     true;
    margin:                      0px;
    padding:                     0px;
    border:                      1px 1px 2px 1px solid;
    location:                    center;
    border-radius:               40px;
    border-color:                @purple;
    background-color:            @background;
    cursor:                      "default";
}

/*****----- Main Box -----*****/
mainbox {
    enabled:                     true;
    spacing:                     20px;
    margin:                      0px;
    padding:                     20px;
    border:                      0px solid;
    border-radius:               0px 0px 0px 0px;
    border-color:                @purple;
    background-color:            transparent;
    children:                    [ "inputbar", "listview" ];
}

/*****----- Inputbar -----*****/
inputbar {
    enabled:                     true;
    spacing:                     10px;
    margin:                      0px;
    padding:                     12px 16px;
    border:                      2px 0px 2px 0px;
    border-radius:               100%;
    border-color:                @purple;
    background-color:            @selection;
    text-color:                  @foreground;
    children:                    [ "prompt", "entry" ];
}

prompt {
    enabled:                     true;
    background-color:            inherit;
    text-color:                  inherit;
}
textbox-prompt-colon {
    enabled:                     true;
    expand:                      false;
    str:                         "::";
    background-color:            inherit;
    text-color:                  inherit;
}
entry {
    enabled:                     true;
    background-color:            inherit;
    text-color:                  inherit;
    cursor:                      text;
    placeholder:                 "Search";
    placeholder-color:           inherit;
}

/*****----- Listview -----*****/
listview {
    enabled:                     true;
    columns:                     4;
    lines:                       4;
    cycle:                       true;
    dynamic:                     true;
    scrollbar:                   false;
    layout:                      vertical;
    reverse:                     false;
    fixed-height:                true;
    fixed-columns:               true;
    
    spacing:                     5px;
    margin:                      0px;
    padding:                     0px;
    border:                      0px solid;
    border-radius:               0px;
    border-color:                @purple;
    background-color:            transparent;
    text-color:                  @foreground;
    cursor:                      "default";
}
scrollbar {
    handle-width:                5px ;
    handle-color:                @purple;
    border-radius:               0px;
    background-color:            @selection;
}

/*****----- Elements -----*****/
element {
    enabled:                     true;
    spacing:                     10px;
    margin:                      0px;
    padding:                     10px;
    border:                      0px solid;
    border-radius:               0px;
    border-color:                @purple;
    background-color:            transparent;
    text-color:                  @foreground;
    orientation:                 vertical;
    cursor:                      pointer;
}
element normal.normal {
    background-color:            transparent;
    text-color:                  @foreground;
}
element selected.normal {
    border:                      0px 0px 2px 0px;
    border-radius:               12px;
    border-color:                @purle;
    background-color:            @selection;
    text-color:                  @green;
}
element-icon {
    background-color:            transparent;
    text-color:                  inherit;
    size:                        32px;
    cursor:                      inherit;
}
element-text {
    background-color:            transparent;
    text-color:                  inherit;
    highlight:                   inherit;
    cursor:                      inherit;
    vertical-align:              0.5;
    horizontal-align:            0.5;
}

/*****----- Message -----*****/
error-message {
    padding:                     20px;
    border:                      0px solid;
    border-radius:               0px;
    border-color:                @red;
    background-color:            black / 10%;
    text-color:                  @foreground;
}
textbox {
    background-color:            transparent;
    text-color:                  @foreground;
    vertical-align:              0.5;
    horizontal-align:            0.0;
    highlight:                   none;
}
